import React, { useEffect, useState } from 'react';

const ShoeForm = props => {
    const [bins, setBins] = useState([]);
    const [modelName, setModelName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [bin, setBin] = useState('');
    

    const handleModelNameChange = (event) => {
        const value = event.target.value
        setModelName(value);
    }
    const handleManufacturerChange = (event) => {
        const value = event.target.value
        setManufacturer(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value);
    }
    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setPictureUrl(value);
    }

    const handleBinChange = (event) => {
        const value = event.target.value
        setBin(value);
    }

    const resetState = () => {
        setModelName('');
        setManufacturer('');
        setColor('');
        setPictureUrl('');
        setBin('');
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.model_name = modelName;
        data.manufacturer = manufacturer;
        data.color = color;
        data.picture_url = pictureUrl;
        data.bin = bin;

        console.log(data)
        const bin_id_from_select = document.getElementById("bin");
        const value = bin_id_from_select.value;

        const shoeUrl = `http://localhost:8080/api/bins/${value}/shoes/`;
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json'
          },
        };

        const response = await fetch(shoeUrl, fetchConfig)
        if (response.ok) {
          const newShoe = await response.json();
          console.log(newShoe)
          resetState();
        }
    }
    

    const fetchData = async () => {
    const url = 'http://localhost:8100/api/bins/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setBins(data.bins)
      
      }
    }
    

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange} value={manufacturer} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleModelNameChange} value={modelName} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control"/>
                <label htmlFor="model_name">Model Name</label>
              </div>
              <label htmlFor="picture_url">Shoe Picture URL</label>
                <div className="input-group mb-3">
                <div className="input-group-prepend">
                    <span className="input-group-text" id="picture_url_span">200 Character limit</span>
                </div>
                <input onChange={handlePictureUrlChange} value={pictureUrl} type="text" className="form-control" id="picture_url" aria-describedby="picture_url"/>
                </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color} placeholder="color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="mb-3">
                <select onChange={handleBinChange} value={bin} required name="bin" id="bin" className="form-select">
                <option value="">Choose a bin</option>
                    {bins.map(bin => {
                    return (
                        <option value={bin.id} key={bin.id}>
                            {bin.id}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  )
}

export default ShoeForm;