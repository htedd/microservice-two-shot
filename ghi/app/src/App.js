import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatForm from './HatForm';
import ShoeForm from './ShoeForm';
import HatList from './HatList';
import ShoeList from './ShoeList';
import React from 'react';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="shoes">
            <Route path="" element={<ShoeList shoes={props.shoes} />} />
            <Route path="new" element={<ShoeForm />} />
          </Route>
          <Route path="hats">
            <Route path="" element={<HatList hats={props.hats} />} />
            <Route path="new" element={<HatForm />} />
          </Route>
        </Routes>
    </BrowserRouter>
  );
}

export default App;
