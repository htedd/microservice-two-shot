import { React, useEffect, useState } from "react";

function HatColumn(props) {
    return (
        <div className="col">
        {props.hats.map(data => {
            return (
                <div key={data.id} className="card mb-3 shadow text-center">
                    <img src={data.url} className="card-img-top" />
                    <div className="card-body">
                        <h5 className="card-title">{data.style_name}</h5>
                        <h6 className="card-subtitle mb-2 text-muted">
                            Fabric: {data.fabric}
                        </h6>
                        <h6 className="card-subtitle mb-2 text-muted">
                            Color: {data.color}
                        </h6>
                        <h6 className="card-subtitle mb-2 text-muted">
                            Location: {data.location.closet_name}
                        </h6>
                    </div>
                    <div className="card-footer">
                        <button onClick={async () => {
                            const hatUrl = `http://localhost:8090/api/hats/${data.id}/`
                            const fetchConfig = {
                                method: "delete",
                            }
                            const response = await fetch (hatUrl, fetchConfig);
                            if (response.ok) {
                                props.onHatDeleted(data.id);
                            }
                        }
                        }
                        className="btn btn-primary">
                            Delete hat
                        </button>
                    </div>
                </div>
            );
        })}
      </div>
    )
}

function HatList(props) {
    const [hatColumns, setHatColumns] = useState([],[],[]);

    const fetchData = async () => {
        const response = await fetch ('http://localhost:8090/api/hats/');
        if (response.ok) {
            const data = await response.json();

            const requests = [];
            for (let hat of data.hats) {
                const detailUrl = `http://localhost:8090/api/hats/${hat.id}/`
                requests.push(fetch(detailUrl));
            }
            const responses = await Promise.all(requests);

            const columns = [[],[],[]];

            let i = 0;
            for (const hatResponse of responses) {
                if (hatResponse.ok) {
                    const details = await hatResponse.json();
                    columns[i].push(details);
                    i = i + 1;
                    if (i > 2) {
                        i = 0;
                    }
                } else {
                    console.error(hatResponse);
                }
            }
            setHatColumns(columns);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const handleHatDeleted = (hatId) => {
        const updatedColumns = hatColumns.map((column) => {
            return column.filter((hat) => hat.id !== hatId);
        });
        setHatColumns(updatedColumns);
    }

    return (
        <div className="container">
        <h2>List of hats</h2>
        <div className="row">
          {hatColumns.map((hatList, index) => {
            return (
              <HatColumn key={index} hats={hatList} onHatDeleted={handleHatDeleted}/>
            );
          })}
        </div>
      </div>
    )

}

export default HatList
