
import { useEffect, useState } from "react";


function ShoeList(props) {
     const [shoes, setShoes] = useState([]);

     const fetchData = async () => {
         const url = "http://localhost:8080/api/shoes/";
         const response = await fetch(url);
         if (response.ok) {
             const data = await response.json();
             setShoes(data.shoes);
         }
     }

   useEffect(() => {
     fetchData();
   }, []);

     const handleDelete = async (id) => {
         const url = `http://localhost:8080/api/shoes/${id}/`
         const response = await fetch(url, { method: "DELETE" });
         if (response.ok) {
             fetchData();
         } else {
             console(`Failed to delete shoe with ID ${id}`);
         }
     }

     return (

     <div className="container">
         <h2>Shoe collection</h2>
         <div></div>
         <div></div>
         <div className="row">
             <div className="col-sm">
             <table className="table table-striped">
                 <thead>
                 <tr>
                     <th>Manufacturer</th>
                     <th>Model Name</th>
                     <th>Color</th>
                     <th>Picture</th>
                     <th>Bin</th>
                     <th>Edit</th>
                 </tr>
                 </thead>
                 <tbody>
                 {shoes.map(shoe => {
                     return (
                     <tr key={shoe.id}>
                         <td>{ shoe.manufacturer }</td>
                         <td>{ shoe.model_name }</td>
                         <td>{ shoe.color }</td>
                         <td>
                             <img
                             src={ shoe.picture_url }
                             className="img-thumbnail"
                             alt="new"
                             />
                         </td>
                         <td>{shoe.bin}</td>
                         <td>
                             <button
                                 className="btn btn-danger"
                                 onClick={() => handleDelete(shoe.id)}
                             >
                                 Delete
                             </button>
                         </td>
                     </tr>
                     );
                 })}
                 </tbody>
             </table>
             </div>
             <div className="col-sm">
             </div>
         </div>
     </div>
     );
   }


export default ShoeList;
