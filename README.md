# Wardrobify

Team:

* Person 1 - Ted Hwang (Hats)
* Person 2 - Kevin Ou (Shoes)

## Design

## Shoes microservice

Created working RESTful Shoe_rest api, using insomnia and django, that used polling to retrieve bin data from the wardrobe microservice. Which all integrated together with React frontend to create working website, able to create/delete/view shoes


## Hats microservice
The hat and locationVO model was created and there can be multiple hats for one location.

Since the location data is stored in the wardrobe microservice, the hats microservice should be unable to make changes to the Location data from the wardrobe microservice. Therefore, a polling microservice was made to create instances of the LocationVO for the hat microservice.

The models and wardwrobe microservice was integreted together to create a single-page application using React by implementing RESTful apis for getting, creating, and deleting hats.
